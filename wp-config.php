<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'mywordpress' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.QQ$Mm~Tqm:NV:v:Yb%]JK+R{ZV@`;o2i/W,j1-VW!VOU<1/N6`dO0eWW87JN=6F');
	define('SECURE_AUTH_KEY',  ':_>t}?{WQw>p*lb5^(CwsT+V,FEktK!&A*rC5ZyP3HW~n(sv#S Dbz][+?eLRdmd');
	define('LOGGED_IN_KEY',    '+@LMe]S2I V&W#4#yhW{iY( ;0KDT$+X6`#L]DY9$ITn3MY!.8#aG~m>HZiKN!KY');
	define('NONCE_KEY',        '/^Of.U_x|<M!]Koj6%AFqKAW-l<ayM@Sn<5}Rj*b`ovM5GV`>ph#yWw|=7@,4<$K');
	define('AUTH_SALT',        'eVVP89]^CO Q`wn?A3Y^s$/B!~=a^]+>_l)z!jcN2q0f]wh(HcW?.NV [>F_ P{R');
	define('SECURE_AUTH_SALT', ',+w|~xMDFgZDj(QNX-XG]6 @rk.,0DxL[&hCyDX@`W(K{T4nabLB@ga/:a`-f9^W');
	define('LOGGED_IN_SALT',   '$gg!U8/ED^?/ExB+!pKO6*>-|0/5RP<,~[ewyD7P^Jb$eBfSz-Ab%H^0;_AK<hkB');
	define('NONCE_SALT',       ' DK.OzVcccB]BK< qkzmt4}g=YzApnC^<yYK@^@ke5+x|]|8Y.p:K3$H/YwCnNN&');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
